import unittest
from src.captcha import Captcha

class TestCaptcha(unittest.TestCase):

    def test_p2_left_should_be_1(self):
        captcha=Captcha(pattern=2, left="one", operand="+", right=1)
        assert(captcha.getLeft()==1)

    def test_p2_left_should_be_5(self):
        captcha=Captcha(pattern=2, left="five", operand="+", right=1)
        assert(captcha.getLeft()==5)

    def test_p2_left_should_be_9(self):
        captcha=Captcha(pattern=2, left="nine", operand="+", right=1)
        assert(captcha.getLeft()==9)

    def test_p2_operand_should_be_1(self):
        captcha=Captcha(pattern=2, left="nine", operand="+", right=1)
        assert(captcha.getOperand()==1)

    def test_p2_operand_should_be_2(self):
        captcha=Captcha(pattern=2, left="nine", operand="-", right=1)
        assert(captcha.getOperand()==2)

    def test_p2_operand_should_be_3(self):
        captcha=Captcha(pattern=2, left="nine", operand="x", right=1)
        assert(captcha.getOperand()==3)

    def test_p2_right_should_be_1(self):
        captcha=Captcha(pattern=2, left="nine", operand="x", right=1)
        assert(captcha.getRight()==1)

    def test_p2_right_should_be_5(self):
        captcha=Captcha(pattern=2, left="nine", operand="x", right=5)
        assert(captcha.getRight()==5)

    def test_p2_right_should_be_9(self):
        captcha=Captcha(pattern=2, left="nine", operand="x", right=9)
        assert(captcha.getRight()==9)
