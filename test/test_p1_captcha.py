import unittest
from src.captcha import Captcha

class TestCaptcha(unittest.TestCase):
    def test_p1_left_should_be_1(self):
        captcha=Captcha(pattern=1, left=1, operand="+", right="one")
        assert(captcha.getLeft()==1)

    def test_p1_left_should_be_5(self):
       captcha=Captcha(pattern=1, left=5, operand="+", right="one")
       assert(captcha.getLeft()==5)
    
    def test_p1_left_should_be_9(self):
       captcha=Captcha(pattern=1, left=9, operand="+", right="one")
       assert(captcha.getLeft()==9) 

    def test_p1_operand_should_be_1(self):
       captcha=Captcha(pattern=1, left=9, operand="+", right="one")
       assert(captcha.getOperand()==1)  

    def test_p1_operand_should_be_2(self):
       captcha=Captcha(pattern=1, left=9, operand="-", right="one")
       assert(captcha.getOperand()==2)  

    def test_p1_operand_should_be_3(self):
       captcha=Captcha(pattern=1, left=9, operand="x", right="one")
       assert(captcha.getOperand()==3)  

    def test_p1_right_should_be_1(self):
       captcha=Captcha(pattern=1, left=9, operand="x", right="one")
       assert(captcha.getRight()==1) 

    def test_p1_right_should_be_5(self):
       captcha=Captcha(pattern=1, left=9, operand="x", right="five")
       assert(captcha.getRight()==5)  

    def test_p1_right_should_be_9(self):
       captcha=Captcha(pattern=1, left=9, operand="x", right="nine")
       assert(captcha.getRight()==9) 
