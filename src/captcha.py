class Captcha(object):
    numbers=["","one","two","three","four","five","six","seven","eight","nine"]

    def __init__(self, pattern, left, operand, right):
        self.pattern=pattern
        self.left=left
        self.operand=operand
        self.right=right 
        pass

    def getLeft(self):
        if self.pattern==2:
            return self.numbers.index(self.left)
        return self.left

    def getOperand(self):
        symbols=["","+","-","x"]
        return symbols.index(self.operand)

    def getRight(self):
        if self.pattern==2:
            return self.right
        return self.numbers.index(self.right)

